
package model;

import java.awt.Image;
import java.awt.Toolkit;
import java.io.File;

/**
 * @purpose
 * 
 * The Class directs a deck of cards of varying sizes used during
 * the game to deal to players.
 *
 * @author Luisa Molina
 * @author Erica Kok
 * 
 * @dueDate 3/21/16
 */

import java.util.Collections;
import java.util.Vector;

public class Deck
{
	private Vector<Card> myDeck;
	private int myFullDeckSize = 52;
	
	/**
	 * Constructor, creates a new Deck with 52 Cards.
	 * 
	 * @author Erica Kok
	 * @author Luisa Molina
	 */

	public Deck()
	{
		myDeck = new Vector<Card>(myFullDeckSize);
		boolean constructed = this.constructDeck() == true;
		if(!constructed)
		{
			myDeck.clear();
		}
	}

	/**
	 * Method to create a deck of 52 unique cards with a type and suit.
	 * 
	 * @return boolean whose value is determined by the size of the deck.
	 * 
	 * @author Erica Kok
	 * @author Luisa Molina
	 */
	public boolean constructDeck()
	{
		for (CardSuit s : CardSuit.values())
		{
			for (CardType t : CardType.values())
			{
				Image cardImage = findImage(s.getSuit().charAt(0), t.getType());
				myDeck.add(new Card(s, t, cardImage));
			}
		}
		return myDeck.size() == 52;
	}

	public Image findImage(char s, int t)
	{
		File file = new File("cards\\" + t + s +".GIF");
		String abs = file.getAbsolutePath();
		Image image = Toolkit.getDefaultToolkit().getImage(abs);
		return image;
	}
	
	/**
	 * Method to take the last card from the deck return it to be used 
	 * in poker model, and removes it from the deck.
	 * 
	 * @return Card drawn from the deck.
	 * 
	 * @author Erica Kok
	 * @author Luisa Molina
	 */
	
	public Card draw()
	{
		if (myDeck.size() > 0)
		{
			Card drawn = myDeck.lastElement();
			myDeck.remove(drawn);
			return drawn;
		}
		return null;
	}

	/**
	 * Method to shuffle the deck in a random order.
	 * 
	 * @return true if the Deck was shuffled, else return false.
	 * 
	 * @author Erica Kok
	 * @author Luisa Molina
	 */
	
	public boolean shuffle()
	{
		Vector<Card> myOriginal = new Vector<Card>();
		myOriginal = myDeck;
		Collections.shuffle(myDeck);
		return !(myDeck == myOriginal);
	}
	
	/**
	 * Method to get the full size of the deck.
	 * 
	 * @return the full size of the deck
	 * 
	 * @author Erica Kok
	 * @author Luisa Molina
	 */

	public int getFullDeckSize()
	{
		return myFullDeckSize;
	}
	
	/**
	 * Method to get the vector of Cards in the Deck.
	 * 
	 * @return vector of Cards in Deck
	 * 
	 * @author Erica Kok
	 * @author Luisa Molina
	 */

	public Vector<Card> getCards()
	{
		return myDeck;
	}
	
	/**
	 * Method to print the whole Deck of Cards as a string.
	 * 
	 * @return string of all Cards in Deck
	 * 
	 * @author Erica Kok
	 * @author Luisa Molina
	 */

	public String toString()
	{
		return "Deck: " + myDeck;
	}
	
	/**
	 * Method to clone the whole Deck.
	 * 
	 * @return clone of the Deck
	 * 
	 * @author Erica Kok
	 * @author Luisa Molina
	 */

	public Object clone()
	{
		Deck myClone = new Deck();
		myClone.getCards().removeAllElements();
		for(Card o: myDeck)
		{
			myClone.getCards().add(o);
		}
		return myClone;
	}
}
