package model;

/**
 * @purpose
 * Enumeration Class for the ranking of each PokerHand.
 * 
 * @author Erica Kok
 * @author Luisa Molina
 * 
 * @dueDaate 3/21/16
 */

public enum PokerHandRanking
{
	HIGH_CARD (1), 
	PAIR (2), 
	TWO_PAIR (3), 
	THREE_OF_KIND (4), 
	STRAIGHT (5), 
	FLUSH (6), 
	FULL_HOUSE (7), 
	FOUR_OF_KIND (8), 
	STRAIGHT_FLUSH (9), 
	ROYAL_FLUSH (10);

	private int myPokerHandRanking;
	
	/**
	 * Constructor, creates a new PokerHandRanking with an integer representing the hierarchy.
	 * 
	 * @param rank
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	private PokerHandRanking(int rank)
	{
		myPokerHandRanking = rank;
	}
	
	/**
	 * Method to get the Rank of a PokerHand.
	 * 
	 * @return the rank of a PokerHand
	 * 
	 * @author Luisa Molina
	 * @author Erica Kok
	 */

	public int getRank()
	{
		return myPokerHandRanking;
	}
}
