package model;

/**
 * @purpose
 * 
 * Enumeration Class for the type of each Card.
 *
 * @author Luisa Molina
 * @author Erica Kok
 * 
 * @dueDate 3/21/16
 */

public enum CardType
{
	TWO (2),
	THREE (3),
	FOUR (4),
	FIVE (5),
	SIX (6),
	SEVEN (7),
	EIGHT (8),
	NINE (9),
	TEN (10),
	JACK (11),
	QUEEN (12),
	KING (13),
	ACE (14);

	private final int myType;
	
	/**
	 * Constructor, creates a new CardType.
	 * 
	 * @param type of card
	 * 
	 * @author Erica Kok
	 * @author Luisa Molina
	 */

	private CardType (int type)
	{
		myType = type;
	}
	
	/**
	 * Method to get the type of the Card.
	 * 
	 * @return type of the Card as an integer.
	 * 
	 * @author Erica Kok
	 * @author Luisa Molina
	 */

	public int getType()
	{
		return myType;
	}

}

