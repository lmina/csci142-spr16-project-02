package model;

/**
 * @purpose
 * 
 * Class for a rough AI player within a poker game,
 * requires an input of type String for the name of the AI.
 *
 * @author Luisa Molina
 * @author Erica Kok
 * 
 * @dueDate 3/21/16
 */

import java.util.Vector;
/**
 * Constructor, creates a new ComputerPlayer with a name.
 * 
 * @param name
 * 
 * @author Erica Kok
 * @author Luisa Molina
 */
public class ComputerPlayer extends Player
{
	public ComputerPlayer()
	{
		super();
		myAmAI = true;
	}
	
	public ComputerPlayer(String name)
	{
		super(name);
		myAmAI = true;
	}

	/**
	 * Method to determine the cards to be discarded based on the hand
	 * the computer player has.
	 * 
	 * @return vector of the indices of the discarded cards.
	 * 
	 * @author Erica Kok
	 * @author Luisa Molina
	 */

	public Vector<Integer> selectCardsToDiscard()
	{
		Vector<Integer> myDiscards = new Vector<Integer>();

		/*
		 * ComputerPlayer checks to see if its hand is a Royal Flush, Straight Flush, ... , or a Straight.
		 * If yes, it doesn't discard anything from the hand.
		 */

		if (myHand.isRoyalFlush() || myHand.isStraightFlush() || myHand.isFourOfKind() || myHand.isFullHouse() || myHand.isFlush() || myHand.isStraight())
		{
			return myDiscards;
		}

		/*
		 * ComputerPlayer checks to see if its hand is a Three of a Kind.
		 * If yes, it checks to see depending on which position the Three of Kind is at, it discards the
		 * other 2 that are not part of the Three of Kind.
		 */

		else if (myHand.isThreeOfKind())
		{
			if (myHand.getCards().get(0).getType().equals(myHand.getCards().get(1).getType()))
			{
				myDiscards.add(3);
				myDiscards.add(4);
				myHand.discard(myDiscards);
			}
			else if (myHand.getCards().get(1).getType().equals(myHand.getCards().get(2).getType()))
			{
				myDiscards.add(0);
				myDiscards.add(4);
				myHand.discard(myDiscards);
			}
			else
			{
				myDiscards.add(0);
				myDiscards.add(1);
				myHand.discard(myDiscards);
			}	
			return myDiscards;		
		}

		/*
		 * ComputerPlayer checks to see if its hand is a Two Pair.
		 * If yes, it discards the other 1 card not part of the Two Pair depending on the
		 * position of that 1 card.
		 */

		else if (myHand.isTwoPair())
		{
			if(myHand.getCards().get(0).getType().equals(myHand.getCards().get(1).getType())
					&& myHand.getCards().get(2).getType().equals(myHand.getCards().get(3).getType()))
			{
				myDiscards.add(4);
				myHand.discard(myDiscards);
			}
			else if(myHand.getCards().get(0).getType().equals(myHand.getCards().get(1).getType())
					&& myHand.getCards().get(3).getType().equals(myHand.getCards().get(4).getType()))
			{
				myDiscards.add(2);
				myHand.discard(myDiscards);
			}
			else 
			{
				myDiscards.add(0);
				myHand.discard(myDiscards);
			}
			return myDiscards;
		}

		/*
		 * ComputerPlayer checks to see if its hand is a Pair.
		 * If yes, it discards the other 3 cards not part of the Pair depending on the position
		 * of the 3 other cards.
		 */

		else if (myHand.isPair())
		{
			if(myHand.getCards().get(0).getType().equals(myHand.getCards().get(1).getType()))
			{
				myDiscards.add(2);
				myDiscards.add(3);
				myDiscards.add(4);
				myHand.discard(myDiscards);
			}
			else if(myHand.getCards().get(1).getType().equals(myHand.getCards().get(2).getType()))
			{
				myDiscards.add(0);
				myDiscards.add(3);
				myDiscards.add(4);
				myHand.discard(myDiscards);
			}
			else
			{
				myDiscards.add(1);
				myDiscards.add(2);
				myDiscards.add(3);
				myHand.discard(myDiscards);
			}
			return myDiscards;
		}

		/*
		 * ComputerPlayer checks to see if its hand is a High Card.
		 * If yes, it discards the lowest 3 cards in the hand.
		 */

		else if(myHand.isHighCard())
		{
			myDiscards.add(0);
			myDiscards.add(1);
			myDiscards.add(2);
			myHand.discard(myDiscards);
		}
		return myDiscards;
	}
}