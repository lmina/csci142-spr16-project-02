package mvc;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.lang.reflect.*;
import java.util.Collections;
import java.util.Vector;

import javax.swing.BorderFactory;
import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.border.Border;

/**
 * Class used to create the user interface for the player to play poker.
 * 
 * @author lulu
 *
 * Date due: March 31, 2016
 */
@SuppressWarnings("serial")
public class View extends Frame
{
	private JButton myStart;
	private ButtonListener myButtonListener;
	private Controller myController;
	private Box myTop = new Box(BoxLayout.X_AXIS);
	private Box myCompCards = new Box(BoxLayout.X_AXIS);
	private Box myCenter = new Box(BoxLayout.X_AXIS);
	private Box myPlayerCards = new Box(BoxLayout.X_AXIS);
	private Box myBottom = new Box(BoxLayout.X_AXIS);
	private JLabel myName1;
	private JLabel myName2;
	private JLabel myHand;
	private JLabel[] myCardHolder;
	private JLabel[] myAICardHolder;
	private ImageIcon[] myCard;
	private ImageIcon[] myAICard;
	private ButtonListener[] myCardSelected;

	public View(Controller controller)
	{
		this.setTitle("Poker Game");
		this.setSize(900, 900);
		this.setBackground(Color.CYAN);
		this.setLayout(new  GridLayout(5, 1, 10, 10));
		this.setVisible(true);
		
		myController = controller;

		myStart = new JButton("Start Game");
		myCard = new ImageIcon[5];
		myAICard = new ImageIcon[5];
		myCardHolder = new JLabel[5];
		myAICardHolder = new JLabel[5];
		myCardSelected = new ButtonListener[5];
		
		this.add5BackOfCardsAI(myCompCards);
		this.add5BackOfCards(myPlayerCards);

		this.associateListeners();

		myName1 = new JLabel();
		myName1.setText("No Name");
		
		JLabel empty = new JLabel();
		empty.setText("                      ");

		myName2 = new JLabel();
		myName2.setText("No Name AI");
		
		myHand = new JLabel();
		this.handText(0);

		myStart.addMouseListener(myButtonListener);
		myTop.add(myName1);
		myTop.add(myName2);
		myCenter.add(myStart);
		myCenter.add(empty);
		myCenter.add(myHand);
		this.add(myTop);
		this.add(myCompCards);
		this.add(myCenter);
		this.add(myPlayerCards);
		this.add(myBottom);
		this.pack();
		this.addWindowListener(new WindowAdapter()
		{
			@Override
			public void windowClosing(WindowEvent e)
			{
				System.exit(0);
			}
		});
	}

	/**
	 * @param box
	 */
	public void add5BackOfCards(Box box)
	{
		for(int x = 0; x < 5; x++)
		{
			myCard[x] = this.cardBack();
			myCardHolder[x] = new JLabel(myCard[x]);
			myCardHolder[x].setMinimumSize(new Dimension(80,100));
			myCardHolder[x].setPreferredSize(new Dimension(80,100));
			myCardHolder[x].setMaximumSize(new Dimension(80,100));
			box.add(myCardHolder[x]);
			System.out.println();
		}
	}

	/**
	 * @param box
	 */
	public void add5BackOfCardsAI(Box box)
	{
		for(int x = 0; x < 5; x++)
		{
			myAICard[x] = this.cardBack();
			myAICardHolder[x] = new JLabel(myAICard[x]);
			myAICardHolder[x].setMinimumSize(new Dimension(80,100));
			myAICardHolder[x].setPreferredSize(new Dimension(80,100));
			myAICardHolder[x].setMaximumSize(new Dimension(80,100));
			box.add(myAICardHolder[x]);
			System.out.println();
		}
	}

	/**
	 * @return
	 */
	public ImageIcon cardBack()
	{
		File file = new File("cards\\back.GIF");
		String abs = file.getAbsolutePath();
		Image img = Toolkit.getDefaultToolkit().getImage(abs);
		ImageIcon back = new ImageIcon(img);
		return back;
	}

	/**
	 * 
	 */
	public void atStart()
	{
		myStart.removeMouseListener(myButtonListener);
		myStart.setText("Discard");
		myName1.setText(this.getPlayerName(0));
		myName2.setText(this.getPlayerName(1));
	}

	/**
	 * @param y
	 */
	public void changePlayerCards(int y)
	{
		myPlayerCards.removeAll();
		myCardHolder = new JLabel[5];
		myCard = new ImageIcon[5];
		while(myCardHolder[4] == null)
		{
			for(int x = 0; x < 5; x++)
			{
				if(myCardHolder[x] == null)
				{
					myCard[x] = this.cardImage(x, y);
					myCardHolder[x] = new JLabel(myCard[x]);
					myCardHolder[x].setMinimumSize(new Dimension(80,100));
					myCardHolder[x].setPreferredSize(new Dimension(80,100));
					myCardHolder[x].setMaximumSize(new Dimension(80,100));
					myCardHolder[x].removeMouseListener(myCardSelected[x]);
					myPlayerCards.add(myCardHolder[x]);
				}
			}
		}
		this.associateListeners();
	}

	/**
	 * @param index
	 * @param player
	 * @return
	 */
	public ImageIcon cardImage(int index, int player)
	{
		Image image = myController.getCardImage(index, player);
		ImageIcon cardI =  new ImageIcon(image);
		return cardI;
	}

	/**
	 * @param label
	 */
	public void isSelected(JLabel label)
	{
		if(label.getBorder() == null)
		{
			Border border = BorderFactory.createRaisedBevelBorder();
			label.setBorder(border);
		}
		else
		{
			label.setBorder(null);
		}
	}

	/**
	 * 
	 */
	public void associateListeners()
	{
		String error;
		Class<? extends Controller> controllerClass;
		Class<?>[] classArgs;
		Object[] args;
		Method startGameMethod, discardCardMethod, cardSelectedMethod;

		controllerClass = myController.getClass();

		error = null;
		startGameMethod = null;
		discardCardMethod = null;
		cardSelectedMethod = null;

		classArgs = new Class[2];

		try
		{
			classArgs[0] = Class.forName("java.lang.Integer");
			classArgs[1] = Class.forName("javax.swing.JLabel");
		}
		catch(ClassNotFoundException e)
		{
			error = e.toString();
			System.out.println(error);
		}

		try
		{
			startGameMethod = controllerClass.getMethod("startGame", (Class<?>[])null);
			discardCardMethod = controllerClass.getMethod("discard", (Class<?>[])null);
			cardSelectedMethod = controllerClass.getMethod("toggleCard", classArgs);
		}
		catch(NoSuchMethodException exception)
		{
			error = exception.toString();
			System.out.println(error);
		}
		catch(SecurityException exception)
		{
			error = exception.toString();
			System.out.println(error);
		}

		if(myStart.getText().equals("Start Game"))
		{
			myButtonListener = new ButtonListener(myController, startGameMethod, null);
		}
		else
		{
			myButtonListener = new ButtonListener(myController, discardCardMethod, null);
			myStart.addMouseListener(myButtonListener);
			for(int x = 0; x < 5; x++)
			{
				args = new Object[2];
				args[0] = x;
				args[1] = myCardHolder[x];
				myCardSelected[x] = new ButtonListener(myController, cardSelectedMethod, args);
				myCardHolder[x].addMouseListener(myCardSelected[x]);
			}
		}
	}

	/**
	 * @param remove
	 */
	public void removeCards(Vector<Integer> remove)
	{
		Collections.sort(remove, Collections.reverseOrder());
		for(int x = 0; x < remove.size(); x++)
		{
			int quitar = remove.get(x);
			myCardHolder[quitar].removeAll();
		}
		this.changePlayerCards(0);
	}

	/**
	 * 
	 */
	public void changeAICards()
	{
		myCompCards.removeAll();
		myAICardHolder = new JLabel[5];
		myAICard = new ImageIcon[5];
		while(myAICardHolder[4] == null)
		{
			for(int x = 0; x < 5; x++)
			{
				if(myAICardHolder[x] == null)
				{
					myAICard[x] = this.cardImage(x, 1);
					myAICardHolder[x] = new JLabel(myAICard[x]);
					myAICardHolder[x].setMinimumSize(new Dimension(80,100));
					myAICardHolder[x].setPreferredSize(new Dimension(80,100));
					myAICardHolder[x].setMaximumSize(new Dimension(80,100));
					myCompCards.add(myAICardHolder[x]);
				}
			}
		}
	}
	
	/**
	 * @param n
	 * @return
	 */
	public String getPlayerName(int n)
	{
		return myController.getName(n);
	}
	
	/**
	 * @param x
	 */
	public void handText(int x)
	{
		int rank = myController.findHand(x);
		String text = this.setRankText(rank);
		myHand.setText(text);
	}
	
	/**
	 * @param rank
	 * @return
	 */
	public String setRankText(int rank)
	{
		if(rank == 10)
		{
			return "Royal Flush";
		}
		else if(rank == 9)
		{
			return "Straight Flush";
		}
		else if(rank == 8)
		{
			return "Four of a Kind";
		}
		else if(rank == 7)
		{
			return "Full House";
		}
		else if(rank == 6)
		{
			return "Flush";
		}
		else if(rank == 5)
		{
			return "Straight";
		}
		else if(rank == 4)
		{
			return "Three of a Kind";
		}
		else if(rank == 3)
		{
			return "Two Pair";
		}
		else if(rank == 2)
		{
			return "Pair";
		}
		else if(rank == 1)
		{
			return "High Card Hand";
		}
		else
		{
			return "No Hand";
		}
	}
}
