package mvc;

import java.awt.Image;
import java.util.Vector;

import javax.swing.JButton;
import javax.swing.JLabel;
import javax.swing.JOptionPane;

import model.*;

/**
 * Class is used to be the liason between the model package and the View class. The main method is used to call the constructor.
 * 
 * @author lulu
 *
 * Date due: March 31, 2016
 */
public class Controller
{
	public PokerModel myGame;
	public View myView;
	private Vector<Integer> myDiscard;
	private Player myPlayer;
	private int myCount;
	private String myNewPlayer;

	public static void main(String[] args)
	{
		new Controller();
	}

	public Controller()
	{
		myView = new View(this);
		myPlayer = this.newPlayer();
		myCount = 0;
	}

	public Player newPlayer()
	{
		myNewPlayer = JOptionPane.showInputDialog("What is your name?");
		Player player = new Player(myNewPlayer);
		return player;
	}

	public void startGame()
	{
		myGame = new PokerModel(myPlayer);
		myGame.dealCards();
		myView.atStart();
		myView.changePlayerCards(0);
		myView.handText(0);
	}

	public void toggleCard(Integer index, JLabel label)
	{
		int x = (int) index;
		Card card = myGame.getPlayer(0).getHand().getCards().get(x);
		if(myCount < 3 || card.isSelected())
		{
			if(card.isSelected())
			{
				myCount --;
			}
			else if(!card.isSelected())
			{
				myCount++;
			}
			card.toggleSelected();
			myView.isSelected(label);
		}
	}

	/**
	 * Method to discard the cards chosen from the deck and remove them from the JLabel that holds the card image.
	 * The decks are filled again and the images are displayed.
	 * only seems to work when the first card of the player is chosen.
	 */
	public void discard()
	{
		Vector<Card> playerCards = myGame.getPlayer(0).getHand().getCards();
		myDiscard = new Vector<Integer>(5);
		for(int x = 0; x < 5; x++)
		{
			if(playerCards.get(x).isSelected())
			{
				myDiscard.add(x);
			}
		}
		myGame.getPlayerUp().getHand().discard(myDiscard);
		this.changePlayer();
		myView.changePlayerCards(0);
		myView.handText(0);
		myView.changeAICards();
		this.winner();
	}

	public void changePlayer()
	{
		myGame.switchTurns();
		ComputerPlayer playerAI = (ComputerPlayer)myGame.getPlayer(1);
		playerAI.selectCardsToDiscard();
		myGame.dealCards();
	}

	public Image getCardImage(int index, int player2)
	{
		Player player = this.getPlayer(player2);
		if(!player.getHand().getCards().get(index).isFaceUp())
		{
			player.getHand().getCards().get(index).flip();
		}
		Image image = player.getHand().getCards().get(index).getImage();
		return image;
	}

	public Image draw(int index, int get)
	{
		Player player = this.getPlayer(get);
		if(!player.getHand().getCards().get(index).isFaceUp())
		{
			player.getHand().getCards().get(index).flip();
		}
		Image image = player.getHand().getCards().get(index).getImage();
		return image;
	}

	public void winner()
	{
		Player winner = this.getWinner();
		JButton close = new JButton("Close");
		int rank = winner.getHand().determineRanking().getRank();
		String winRank = myView.setRankText(rank);
		JOptionPane.showMessageDialog(close, "The winner is " + winner.getName() +" with the hand " + winRank + "!");
	}

	public Player getWinner()
	{
		return myGame.determineWinner();
	}

	public Player getPlayer(int index)
	{
		return myGame.getPlayer(index);
	}

	public int findHand(int x)
	{
		try
		{
			return myGame.getPlayer(x).getHand().determineRanking().getRank();
		}
		catch(NullPointerException e)
		{
			return 0;
		}
	}

	public String getName(int index)
	{
		return myGame.getPlayer(index).getName();
	}
}
